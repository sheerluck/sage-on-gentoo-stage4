FROM sheerluck/sage-on-gentoo-stage4:24.05-py10

ARG FEATURES="-ipc-sandbox -mount-sandbox -network-sandbox -pid-sandbox"
ARG OVERLAY="/var/db/repos/sage-on-gentoo"
ARG GENTOO="/var/db/repos/gentoo"

ENV LANG=C.UTF-8


RUN    rm /etc/portage/make.conf

RUN    echo 'PYTHON_TARGETS="python3_12"'       >> /etc/portage/make.conf \
    && echo 'PYTHON_SINGLE_TARGET="python3_12"' >> /etc/portage/make.conf \
    && echo 'RUBY_TARGETS="ruby32"'             >> /etc/portage/make.conf \
    && echo 'FEATURES="'${FEATURES}'"'          >> /etc/portage/make.conf \
    && echo 'LINGUAS="en"'                      >> /etc/portage/make.conf \
    && echo 'L10N="en"'                         >> /etc/portage/make.conf \
    && echo 'ACCEPT_LICENSE="CeCILL-B"'         >> /etc/portage/make.conf \
    && echo 'USE="${USE} -introspection png"'   >> /etc/portage/make.conf \
    && echo 'en_US.UTF-8 UTF-8'                 >> /etc/locale.gen        \
    && echo '-python_targets_python3_12'        >> /etc/portage/profile/use.stable.mask \
    && echo '-python_single_target_python3_12'  >> /etc/portage/profile/use.stable.mask

RUN    rm /etc/portage/package.mask/python

RUN    echo 'dev-lang/python            ~amd64' >> /etc/portage/package.accept_keywords/zzz \
    && echo '<dev-lang/python-3.12.0'           >> /etc/portage/package.mask/python \
    && echo 'dev-lang/python:3.13'              >> /etc/portage/package.mask/python

RUN    emerge rust-bin
RUN    emerge -DNu --with-bdeps=y --complete-graph=y @system @world  \
    && emerge -C dev-lang/python:3.10

RUN    emerge ExportSageNB beniget cvxopt cypari2 cysignals gast     \
       dev-python/lrcalc dev-python/sphinx fpylll gmpy jupyter-jsmol \
       lark maxima memory-allocator meson-python mpmath networkx     \
       pkgconfig poetry-core pplpy primecountpy pyproject-metadata   \
       pythran rpy scipy snowballstemmer sympy tre tzlocal

RUN    emerge kiwisolver contourpy conway-polynomials cycler matplotlib tomli


# cleanup
RUN    emerge @preserved-rebuild       \
    && eselect news read               \
    && rm -frv /var/cache/distfiles/*  \
    && rm -frv /opt/*                  \
    && mv /usr/share/doc/pari-* /tmp   \
    && rm -frv /usr/share/doc/*        \
    && mv /tmp/pari-* /usr/share/doc   \
    && rm -frv /usr/share/man/*        \
    && rm -frv /tmp/*

WORKDIR /
CMD ["/bin/bash"]

# docker buildx rm insecure-builder

# docker buildx create --config /etc/buildkit/buildkitd.toml              \
#      --use --name insecure-builder                                      \
#      --buildkitd-flags '--allow-insecure-entitlement security.insecure' \
#      --driver-opt env.BUILDKIT_STEP_LOG_MAX_SIZE=-1                     \
#      --driver-opt env.BUILDKIT_STEP_LOG_MAX_SPEED=-1

# docker buildx build --allow security.insecure                \
#                     --progress=plain                         \
#                     --platform=amd64                         \
# --output "type=docker,push=false,name=sheerluck/sage-on-gentoo-stage4:24.05-py12,dest=24.05-py12.tar" \
#                     -f Dockerfile .


# docker load --input 24.05-py12.tar


# not working:
#  -t sheerluck/sage-on-gentoo-stage4:24.05-py12
#  -t sheerluck/sage-on-gentoo-stage4:latest-py11
#  -f Dockerfile .
