
# Since Docker is a US company, we must comply with US export control regulations. In an effort to comply with these, we now block all IP addresses that are located in Cuba, Russia, Iran, North Korea, Republic of Crimea, Sudan, and Syria.
